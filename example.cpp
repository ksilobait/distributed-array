#include <cstdio>
#include <cmath>
#include <mpi.h>

#define Im 40
#define Jm 40
#define Km 40
#define a 1

/* Выделение памяти для 3D пространства для текущей и предыдущей итерации */
double e = 0.00001;
int L0 = 1;
int L1 = 0;


/* Функция определения точного решения */
double Fresh(double x, double y, double z)
{
	double res;
	res = x + y + z;
	return res;
}

/* Функция задания правой части уравнения */
double Ro(double x, double y, double z)
{
	double d;
	d = -a * (x + y + z);
	return d;
}

int main(int argc, char** argv)
{
	MPI_Init(&argc, &argv);
	
	int threadRank;
	int threadCount;
	MPI_Comm_size(MPI_COMM_WORLD, &threadCount);
	MPI_Comm_rank(MPI_COMM_WORLD, &threadRank);
	if (threadRank == 0)
	{
		printf("Thread count: %d\n", threadCount);
	}
	
	int* perThreads = new int[threadCount]();
	int* offsets = new int[threadCount]();
	for (int i = 0, height = Km + 1, tmp = height % threadCount, currentLine = 0; i < threadCount; ++i)
	{
		offsets[i] = currentLine;
		perThreads[i] = i < tmp ? (height / threadCount + 1) : (height / threadCount);
		currentLine += perThreads[i];
	}
	
	int I = perThreads[threadRank];
	int J = (Jm + 1);
	int K = (Km + 1);
	
	double* (F[2]);
	F[0] = new double[I * J * K]();
	F[1] = new double[I * J * K]();
	
	double Xm = 2.0;
	double Ym = 2.0;
	double Zm = 2.0;

	/* Размеры шагов */
	double hx = Xm / Im;
	double hy = Ym / Jm;
	double hz = Zm / Km;
	
	//INIC
	for (int i = 0, startLine = offsets[threadRank]; i <= perThreads[threadRank] - 1; i++, startLine++)
	{
		for (int j = 0; j <= Jm; j++)
		{
			for (int k = 0; k <= Km; k++)
			{
				if ((startLine != 0) && (j != 0) && (k != 0) && (startLine != Im) && (j != Jm) && (k != Km))
				{
					F[0][i * J * K + j * K + k] = 0;
					F[1][i * J * K + j * K + k] = 0;
				}
				else
				{
					F[0][i * J * K + j * K + k] = Fresh(startLine * hx, j * hy, k * hz);
					F[1][i * J * K + j * K + k] = Fresh(startLine * hx, j * hy, k * hz);
				}
			}
		}
	}
	

	int f;
	MPI_Request sendRequest[2] = {};
	MPI_Request recRequest[2] = {};

	double* (buffer[2]);
	buffer[0] = new double[K * J]();
	buffer[1] = new double[K * J]();

	double owx = pow(hx, 2);
	double owy = pow(hy, 2);
	double owz = pow(hz, 2);
	double c = 2 / owx + 2 / owy + 2 / owz + a;

	int iter = 0;


	double start = MPI_Wtime();
	do
	{
		f = 1;
		L0 = 1 - L0;
		L1 = 1 - L1;
		

		//GET EDGES
		if (threadRank != 0)
		{
			MPI_Isend(&(F[L0][0]), K * J, MPI_DOUBLE, threadRank - 1, 0, MPI_COMM_WORLD, &sendRequest[0]); //down
			MPI_Irecv(buffer[0], K * J, MPI_DOUBLE, threadRank - 1, 1, MPI_COMM_WORLD, &recRequest[1]);
		}
		if (threadRank != threadCount - 1)
		{
			MPI_Isend(&(F[L0][(perThreads[threadRank] - 1) * J * K]), K * J, MPI_DOUBLE, threadRank + 1, 1, MPI_COMM_WORLD, &sendRequest[1]); //верх
			MPI_Irecv(buffer[1], K * J, MPI_DOUBLE, threadRank + 1, 0, MPI_COMM_WORLD, &recRequest[0]);
		}

		
		//CENTER
		for (int i = 1; i < perThreads[threadRank] - 1; ++i)
		{
			for (int j = 1; j < Jm; ++j)
			{
				for (int k = 1; k < Km; ++k)
				{
					double Fi = (F[L0][(i + 1) * J * K + j * K + k] + F[L0][(i - 1) * J * K + j * K + k]) / owx;
					double Fj = (F[L0][i * J * K + (j + 1) * K + k] + F[L0][i * J * K + (j - 1) * K + k]) / owy;
					double Fk = (F[L0][i * J * K + j * K + (k + 1)] + F[L0][i * J * K + j * K + (k - 1)]) / owz;
					F[L1][i * J * K + j * K + k] = (Fi + Fj + Fk - Ro((i + offsets[threadRank]) * hx, j * hy, k * hz)) / c;
					if (fabs(F[L1][i * J * K + j * K + k] - Fresh((i + offsets[threadRank]) * hx, j * hy, k * hz)) > e)
					{
						f = 0;
					}
				}
			}
		}

		
		//WAIT
		if (threadRank != 0)
		{
			MPI_Wait(&recRequest[1], MPI_STATUS_IGNORE);
			MPI_Wait(&sendRequest[0], MPI_STATUS_IGNORE);
		}
		if (threadRank != threadCount - 1)
		{
			MPI_Wait(&recRequest[0], MPI_STATUS_IGNORE);
			MPI_Wait(&sendRequest[1], MPI_STATUS_IGNORE);
		}

		
		//CALC EDGES
		for (int j = 1; j < Jm; ++j)
		{
			for (int k = 1; k < Km; ++k)
			{
				if (threadRank != 0)
				{
					int i = 0;
					double Fi = (F[L0][(i + 1) * J * K + j * K + k] + buffer[0][j * K + k]) / owx;
					double Fj = (F[L0][i * J * K + (j + 1) * K + k] + F[L0][i * J * K + (j - 1) * K + k]) / owy;
					double Fk = (F[L0][i * J * K + j * K + (k + 1)] + F[L0][i * J * K + j * K + (k - 1)]) / owz;
					F[L1][i * J * K + j * K + k] = (Fi + Fj + Fk - Ro((i + offsets[threadRank]) * hx, j * hy, k * hz)) / c;
					if (fabs(F[L1][i * J * K + j * K + k] - Fresh((i + offsets[threadRank]) * hx, j * hy, k * hz)) > e)
					{
						f = 0;
					}
				}
				
				if (threadRank != threadCount - 1)
				{
					int i = perThreads[threadRank] - 1;
					double Fi = (buffer[1][j * K + k] + F[L0][(i - 1) * J * K + j * K + k]) / owx;
					double Fj = (F[L0][i * J * K + (j + 1) * K + k] + F[L0][i * J * K + (j - 1) * K + k]) / owy;
					double Fk = (F[L0][i * J * K + j * K + (k + 1)] + F[L0][i * J * K + j * K + (k - 1)]) / owz;
					F[L1][i * J * K + j * K + k] = (Fi + Fj + Fk - Ro((i + offsets[threadRank]) * hx, j * hy, k * hz)) / c;
					
					/*if (threadRank == 0)
					{
						std::cout << fabs(F[L1][i * J * K + j * K + k] - Fresh((i + offsets[threadRank]) * hx, j * hy, k * hz)) << ' ' << std::endl;
					}*/

					if (fabs(F[L1][i * J * K + j * K + k] - Fresh((i + offsets[threadRank]) * hx, j * hy, k * hz)) > e)
					{
						f = 0;
					}
				}
			}
		}

		if (threadRank == 0)
		{
			iter++;
		}

		int tmpF;
		MPI_Allreduce(&f, &tmpF, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
		f = tmpF;
	} while (f == 0);
	

	double finish = MPI_Wtime();
	if (threadRank == 0)
	{
		std::cout << "iterations: " << iter << std::endl;
		printf("TIme: %lf\n", finish - start);
	}
	

	double max = 0.0;
	for (int i = 1; i < perThreads[threadRank] - 2; i++)
	{
		for (int j = 1; j < Jm; j++)
		{
			for (int k = 1; k < Km; k++)
			{
				double fff1 = fabs(F[L1][i * J * K + j * K + k] - Fresh((i + offsets[threadRank]) * hx, j * hy, k * hz));
				if (fff1 > max)
				{
					max = fff1;
				}
			}
		}
	}
	double tmpMax = 0;
	MPI_Allreduce(&max, &tmpMax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	if (threadRank == 0)
	{
		max = tmpMax;
		printf("Max differ = %lf\n", max);
	}

	
	
	delete[] buffer[0];
	delete[] buffer[1];
	delete[] F[0];
	delete[] F[1];
	delete[] offsets;
	delete[] perThreads;
	
	MPI_Finalize();
	return 0;
}
