#include <cmath>

//-------------------------------------------------------------------------------------------------------------------------------
//MY LIBRARY
#include <mpi.h>
#include <vector>
#include <thread>

#define my_MPI_TERMINATE (-1)

class DistributedController;

template<class T>
class DistributedArray;

//-------------------------------------------------------------------------------------------------------------------------------

class DistributedBaseArray
{
public:
	virtual void send(std::vector<MPI_Aint> i, int dest, MPI_Aint elementNumber) =0;	
	virtual ~DistributedBaseArray() = default;
};

//DistributedController----------------------------------------------------------------------------------------------------------

class DistributedController
{
private:
	//fields
	bool toStop = false;
	std::vector<DistributedBaseArray *> arrays;
	int rank;
	std::thread *data_thread;
	int ID_Generator = 0;
	
	//methods
	void launch_data_thread();

public:
	//fields
	const int DATA_TAG = 5;
	const int REQUEST_TAG = 6;
	const int ASYNC_DATA_TAG = 2;
	const int ASYNC_REQUEST_TAG = 3;
	MPI_Comm MPI_COMM_ARRAY_LIB;
	
	//methods
	explicit DistributedController(int argc, char* argv[]);
	~DistributedController();
	void terminate();

	void addArray(DistributedBaseArray *an_array_) { arrays.push_back(an_array_); };
	int getRank() { return this->rank; };
	int get_ID_and_increment() { return this->ID_Generator++; };
};


DistributedController::DistributedController(int argc, char* argv[])
{
	int provided;
	MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
	if (provided != MPI_THREAD_MULTIPLE)
	{
		printf("warning: MPI_THREAD_MULTIPLE failed\n");
	}
	
	MPI_Comm_dup(MPI_COMM_WORLD, &MPI_COMM_ARRAY_LIB);
	
	
	MPI_Comm_rank(MPI_COMM_ARRAY_LIB, &rank);
	
	data_thread = new std::thread(&DistributedController::launch_data_thread, this);
}


void DistributedController::launch_data_thread()
{
	while (!toStop)
	{
		//received a request
		MPI_Status status;
		
		int incoming_msg_size;
		
		MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_ARRAY_LIB, &status);
		if (status.MPI_TAG == REQUEST_TAG)
		{
			MPI_Get_count(&status, MPI_BYTE, &incoming_msg_size);
			auto* req = new char[incoming_msg_size];
			MPI_Recv(req, incoming_msg_size, MPI_BYTE, status.MPI_SOURCE, REQUEST_TAG, MPI_COMM_ARRAY_LIB, &status);
			int coords_size = incoming_msg_size / sizeof(MPI_Aint) - 1;
			MPI_Aint* buf = (MPI_Aint*) req;
			
			if (buf[0]== my_MPI_TERMINATE)
			{
				delete[] req;
				break;
			}
			
			std::vector<MPI_Aint> coords;
			for (int i = 0; i < coords_size; i++)
			{
				coords.push_back(buf[i + 1]);
			}
			arrays[buf[0]]->send(coords, status.MPI_SOURCE, 1);
			delete[] req;
		}
		else if (status.MPI_TAG == ASYNC_REQUEST_TAG)
		{
			MPI_Get_count(&status, MPI_BYTE, &incoming_msg_size);
			char* req = new char[incoming_msg_size];
			MPI_Recv(req, incoming_msg_size, MPI_BYTE, status.MPI_SOURCE, ASYNC_REQUEST_TAG, MPI_COMM_ARRAY_LIB, &status);
			int coords_size = incoming_msg_size / sizeof(MPI_Aint) - 2;
			MPI_Aint* buf = (MPI_Aint*) req;
			
			if (buf[0]== my_MPI_TERMINATE)
			{
				delete[] req;
				break;
			}
			
			std::vector<MPI_Aint> coords;
			for (int i = 0; i < coords_size; i++)
			{
				coords.push_back(buf[i + 2]);
			}
			arrays[buf[0]]->send(coords, status.MPI_SOURCE, buf[1]);
			delete[] req;
		}
	}
}

void DistributedController::terminate()
{
	toStop = true;
	
	MPI_Aint req = my_MPI_TERMINATE;
	MPI_Send(&req, sizeof(MPI_Aint), MPI_BYTE, rank, REQUEST_TAG, MPI_COMM_ARRAY_LIB);
	
	data_thread->join();
}

DistributedController::~DistributedController()
{
	delete data_thread;
}


//DistributedArray---------------------------------------------------------------------------------------------------------------

template<class T>
class DistributedArray : public DistributedBaseArray
{
private:
	std::vector<long> sizes;
	std::vector<long> parts; //if second>0 then numberOfParts
	std::vector<std::vector<long>> displacements;
	std::vector<std::vector<long>> sendCounts;
	
	T *data;
	unsigned long dims;
	std::vector<long> theLocalStart;
	std::vector<long> theLocalEnd;
	DistributedController &controller;
	MPI_Aint ID;

	void send(std::vector<long> the_coordinates, int destination, MPI_Aint elementNumber) override;	

public:
	DistributedArray(DistributedController &controller_, std::initializer_list<long> sizes_,
	                 std::initializer_list<long> parts_, std::initializer_list<long> ranks_);
	~DistributedArray() override = default;
	unsigned int whose(std::initializer_list<long> cell_coordinates);
	long localStartInDim(unsigned int dimension_number);
	long localEndInDim(unsigned int dimension_number);
	T &operator[](std::initializer_list<long> the_coordinates);
	T &operator[](std::vector<long> the_coordinates);
	T at(std::initializer_list<long> the_coordinates);
	void asyncRequest(std::initializer_list<long> the_coordinates, int elementNumber, MPI_Request& sendRequest, MPI_Request& recRequest, T* destination);
	
	void deallocate() { MPI_Barrier(controller.MPI_COMM_ARRAY_LIB); delete[] data; };
	std::vector<long> localStart() { return this->theLocalStart; };
	std::vector<long> localEnd() { return this->theLocalEnd; };	
};

template<class T>
DistributedArray<T>::DistributedArray(DistributedController &controller_, std::initializer_list<long> sizes_,
                                      std::initializer_list<long> parts_, std::initializer_list<long> ranks_)
		: controller(controller_)
{
	this->ID = controller.get_ID_and_increment();
	auto i1 = sizes_.begin();
	auto i2 = parts_.begin();
	for (/**/; i1 != sizes_.end() || i2 != parts_.end(); i1++, i2++)
	{
		if (*i1 <= 0)
		{
			throw std::invalid_argument("you requested 0 cells in one of the dimensions");
		}
		
		sizes.push_back(*i1);
		parts.push_back(*i2);
	}
	if (i1 != sizes_.end() || i2 != parts_.end())
	{
		throw std::invalid_argument("dimension lists have different size");
	}
	dims = sizes.size();
	
	auto iter = ranks_.begin();
	long memory_to_allocate = 1;
	for (unsigned int i = 0; i < dims; i++, iter++)
	{
		if (parts[i] <= 1) //don't divide
		{
			memory_to_allocate = memory_to_allocate * sizes[i];
			
			std::vector<long> temp;
			temp.push_back(0);
			displacements.push_back(temp);
			
			std::vector<long> temp2;
			temp2.push_back(sizes[i]);
			sendCounts.push_back(temp2);
			
			this->theLocalStart.push_back(0);
			this->theLocalEnd.push_back(sizes[i]);
		}
		else //divide
		{
			std::vector<long> temp_displacements;
			std::vector<long> temp_sendCounts;
			
			long rem = sizes[i] % parts[i]; // lines remaining after division among processes
			long sumDisplacements = 0;
			for (unsigned int j = 0; j < parts[i]; ++j)
			{
				temp_displacements.push_back(sumDisplacements);

				long sendCountsLocal = sizes[i] / parts[i];
				if (rem > 0)
				{
					sendCountsLocal++;
					rem--;
				}

				if (j == *iter) //want to allocate this part
				{
					memory_to_allocate = memory_to_allocate * sendCountsLocal;
					this->theLocalStart.push_back(sumDisplacements);
					this->theLocalEnd.push_back(sumDisplacements + sendCountsLocal);
				}
				
				temp_sendCounts.push_back(sendCountsLocal);
				sumDisplacements = sumDisplacements + sendCountsLocal;
			}
			
			displacements.push_back(temp_displacements);
			sendCounts.push_back(temp_sendCounts);
		}
	}
	data = new T[memory_to_allocate];
	//std::cout << "allocated " << memory_to_allocate << " by rank #" << controller.getRank() << std::endl;
	controller.addArray(this);
}

template<class T>
long DistributedArray<T>::localStartInDim(unsigned int dimension_number)
{
	if (dimension_number >= this->dims)
	{
		throw std::invalid_argument("you requested invalid dimension");
	}
	
	long local_start = this->theLocalStart[dimension_number];
	long local_end = this->theLocalEnd[dimension_number];
	if (local_start >= local_end)
	{
		return local_end;
	}
	else
	{
		return local_start;
	}
}


template<class T>
long DistributedArray<T>::localEndInDim(unsigned int dimension_number)
{
	if (dimension_number >= this->dims)
	{
		throw std::invalid_argument("you requested invalid dimension");
	}
	
	return this->theLocalEnd[dimension_number];
}


template<class T>
void DistributedArray<T>::send(std::vector<MPI_Aint> the_coordinates, int destination, MPI_Aint elementNumber)
{
	//send an answer with data
	T& fetched = this->operator[](the_coordinates);
	MPI_Send(&fetched, sizeof(T) * elementNumber, MPI_BYTE, destination, controller.ASYNC_DATA_TAG, controller.MPI_COMM_ARRAY_LIB);
}

//fetches local objects
template<class T>
T &DistributedArray<T>::operator[](std::initializer_list<long> the_coordinates)
{
	long index = 0;
	
	int dim = 0;
	for (auto& it : the_coordinates)
	{
		long the_coord = it - localStartInDim(dim);
		long send_count = localEndInDim(dim) - localStartInDim(dim);
		index = index * send_count;
		index = index + the_coord;
		
		dim++;
	}
	
	if (dim != dims)
	{
		throw std::invalid_argument("you typed not enough coordinates to find the cell");
	}
	
	//std::cout << index << std::endl;
	return data[index];
}

template<class T>
T& DistributedArray<T>::operator[](std::vector<long> the_coordinates)
{
	long index = 0;
	
	int dim = 0;
	for (auto& it : the_coordinates)
	{
		long the_coord = it - localStartInDim(dim);
		long send_count = localEndInDim(dim) - localStartInDim(dim);
		index = index * send_count;
		index = index + the_coord;

		dim++;
	}
	
	if (the_coordinates.size() != dims)
	{
		throw std::invalid_argument("you typed not enough coordinates to find the cell");
	}
	
	return data[index];
}


//fetches remote objects
template<class T>
T DistributedArray<T>::at(std::initializer_list<MPI_Aint> the_coordinates)
{
	unsigned int who_has_the_element = whose(the_coordinates);
	if (who_has_the_element != controller.getRank())
	{
		MPI_Aint* req = new MPI_Aint[the_coordinates.size() + 1];
		
		req[0] = this->ID;
		int i = 0;
		for (MPI_Aint s : the_coordinates)
		{
			i++;
			req[i] = s;
		}
		
		MPI_Send(req, sizeof(MPI_Aint) * (the_coordinates.size() + 1), MPI_BYTE, who_has_the_element, controller.REQUEST_TAG, controller.MPI_COMM_ARRAY_LIB);
		delete[] req;
		
		T ans;
		MPI_Recv(&ans, sizeof(T), MPI_BYTE, who_has_the_element, controller.DATA_TAG, controller.MPI_COMM_ARRAY_LIB, MPI_STATUS_IGNORE);
		return ans;
	}
	else
	{
		return this->operator[](the_coordinates);
	}
}

template<class T>
void DistributedArray<T>::asyncRequest(std::initializer_list<MPI_Aint> the_coordinates, int elementNumber, MPI_Request& sendRequest, MPI_Request& recRequest, T* destination)
{
	unsigned int who_has_the_element = whose(the_coordinates);
	MPI_Aint* req = new MPI_Aint[the_coordinates.size() + 2];
	
	req[0] = this->ID;
	req[1] = elementNumber;
	int i = 1;
	for (MPI_Aint s : the_coordinates)
	{
		i++;
		req[i] = s;
	}
	
	MPI_Isend(req, sizeof(MPI_Aint) * (the_coordinates.size() + 2), MPI_BYTE, who_has_the_element, controller.ASYNC_REQUEST_TAG, controller.MPI_COMM_ARRAY_LIB, &sendRequest);
	delete[] req;
	MPI_Irecv(destination, elementNumber * sizeof(T), MPI_BYTE, who_has_the_element, controller.ASYNC_DATA_TAG, controller.MPI_COMM_ARRAY_LIB, &recRequest);
}

template<class T>
unsigned int DistributedArray<T>::whose(std::initializer_list<long> cell_coordinates)
{
	std::vector<long> rank_coordinates;
	int i = 0;
	for (auto it = cell_coordinates.begin(); it != cell_coordinates.end(); it++, i++)
	{
		if (parts[i] > 0)
		{
			auto low = std::lower_bound(displacements[i].begin(), displacements[i].end(), *it);
			int j = low - displacements[i].begin();
			if (displacements[i][j] != *it)
			{
				rank_coordinates.push_back(j - 1);
			}
			else
			{
				rank_coordinates.push_back(j);
			}
		}
		else
		{
			rank_coordinates.push_back(0);
		}
	}
	
	unsigned int the_rank = 0;
	int multiplier = 1;
	for (i = rank_coordinates.size() - 1; i >= 0; i--)
	{
		the_rank = the_rank + rank_coordinates[i] * multiplier;
		multiplier = multiplier * parts[i];
	}
	return the_rank;
}

//-------------------------------------------------------------------------------------------------------------------------------

#define Im 40
#define Jm 40
#define Km 40
#define a 1

/* Выделение памяти для 3D пространства для текущей и предыдущей итерации */
double e = 0.00001;
int L0 = 1;
int L1 = 0;


/* Функция определения точного решения */
double Fresh(double x, double y, double z)
{
	double res;
	res = x + y + z;
	return res;
}

/* Функция задания правой части уравнения */
double Ro(double x, double y, double z)
{
	double d;
	d = -a * (x + y + z);
	return d;
}

int main(int argc, char** argv)
{
	DistributedController controller(argc, argv);
	
	int threadRank;
	int threadCount;
	MPI_Comm_size(MPI_COMM_WORLD, &threadCount);
	MPI_Comm_rank(MPI_COMM_WORLD, &threadRank);
	if (threadRank == 0)
	{
		printf("Thread count: %d\n", threadCount);
	}
	

	int I = (Im + 1);
	int J = (Jm + 1);
	int K = (Km + 1);
	
	DistributedArray<double>* (F[2]);
	F[0] = new DistributedArray<double>(controller, {I, J, K}, {threadCount, 1, 1}, {threadRank, 0, 0});
	F[1] = new DistributedArray<double>(controller, {I, J, K}, {threadCount, 1, 1}, {threadRank, 0, 0});
	
	double Xm = 2.0;
	double Ym = 2.0;
	double Zm = 2.0;

	/* Размеры шагов */
	double hx = Xm / Im;
	double hy = Ym / Jm;
	double hz = Zm / Km;
	
	//INIC
	for (int i = F[0]->localStartInDim(0); i < F[0]->localEndInDim(0); i++)
	{
		for (int j = 0; j <= Jm; j++)
		{
			for (int k = 0; k <= Km; k++)
			{
				if ((i != 0) && (j != 0) && (k != 0) && (i != Im) && (j != Jm) && (k != Km))
				{
					(*F[0])[{i, j, k}] = 0;
					(*F[1])[{i, j, k}] = 0;
				}
				else
				{
					(*F[0])[{i, j, k}] = Fresh(i * hx, j * hy, k * hz);
					(*F[1])[{i, j, k}] = Fresh(i * hx, j * hy, k * hz);
				}
			}
		}
	}
	

	int f;
	MPI_Request sendRequest[2] = {};
	MPI_Request recRequest[2] = {};

	double* (buffer[2]);
	buffer[0] = new double[K * J]();
	buffer[1] = new double[K * J]();

	double owx = pow(hx, 2);
	double owy = pow(hy, 2);
	double owz = pow(hz, 2);
	double c = 2 / owx + 2 / owy + 2 / owz + a;

	int iter = 0;


	double start = MPI_Wtime();
	do
	{
		f = 1;
		L0 = 1 - L0;
		L1 = 1 - L1;
		

		//GET EDGES
		if (threadRank != 0)
		{
			F[L0]->asyncRequest({F[0]->localStartInDim(0) - 1, 0, 0}, K * J, sendRequest[0], recRequest[1], buffer[0]);
		}
		if (threadRank != threadCount - 1)
		{
			F[L0]->asyncRequest({F[0]->localEndInDim(0), 0, 0}, K * J, sendRequest[1], recRequest[0], buffer[1]);
		}


		//CENTER
		for (int i = F[0]->localStartInDim(0) + 1; i < F[0]->localEndInDim(0) - 1; i++)
		{
			for (int j = 1; j < Jm; ++j)
			{
				for (int k = 1; k < Km; ++k)
				{
					double Fi = ((*F[L0])[{(i + 1), j, k}] + (*F[L0])[{(i - 1), j, k}]) / owx;
					double Fj = ((*F[L0])[{i, (j + 1), k}] + (*F[L0])[{i, (j - 1), k}]) / owy;
					double Fk = ((*F[L0])[{i, j, (k + 1)}] + (*F[L0])[{i, j, (k - 1)}]) / owz;
					(*F[L1])[{i, j, k}] = (Fi + Fj + Fk - Ro(i * hx, j * hy, k * hz)) / c;
					if (fabs((*F[L1])[{i, j, k}] - Fresh(i * hx, j * hy, k * hz)) > e)
					{
						f = 0;
					}
				}
			}
		}

		
		//WAIT
		if (threadRank != 0)
		{
			MPI_Wait(&recRequest[1], MPI_STATUS_IGNORE);
			MPI_Wait(&sendRequest[0], MPI_STATUS_IGNORE);
		}
		if (threadRank != threadCount - 1)
		{
			MPI_Wait(&recRequest[0], MPI_STATUS_IGNORE);
			MPI_Wait(&sendRequest[1], MPI_STATUS_IGNORE);
		}

		
		//CALC EDGES
		for (int j = 1; j < Jm; ++j)
		{
			for (int k = 1; k < Km; ++k)
			{
				if (threadRank != 0)
				{
					int i = F[0]->localStartInDim(0);
					double Fi = ((*F[L0])[{(i + 1), j, k}] + buffer[0][j * K + k]) / owx;
					double Fj = ((*F[L0])[{i, (j + 1), k}] + (*F[L0])[{i, (j - 1), k}]) / owy;
					double Fk = ((*F[L0])[{i, j, (k + 1)}] + (*F[L0])[{i, j, (k - 1)}]) / owz;
					(*F[L1])[{i, j, k}] = (Fi + Fj + Fk - Ro(i * hx, j * hy, k * hz)) / c;
					if (fabs((*F[L1])[{i, j, k}] - Fresh(i * hx, j * hy, k * hz)) > e)
					{
						f = 0;
					}
				}
				
				if (threadRank != threadCount - 1)
				{
					int i = F[0]->localEndInDim(0) - 1;
					double Fi = (buffer[1][j * K + k] + (*F[L0])[{(i - 1), j, k}]) / owx;
					double Fj = ((*F[L0])[{i, (j + 1), k}] + (*F[L0])[{i, (j - 1), k}]) / owy;
					double Fk = ((*F[L0])[{i, j, (k + 1)}] + (*F[L0])[{i, j, (k - 1)}]) / owz;
					(*F[L1])[{i, j, k}] = (Fi + Fj + Fk - Ro(i * hx, j * hy, k * hz)) / c;
					
					/*if (threadRank == 0)
					{
						std::cout << fabs((*F[L1])[{i, j, k}] - Fresh((i + F[0]->localStartInDim(0)) * hx, j * hy, k * hz)) << ' ' << std::endl;
					}*/

					if (fabs((*F[L1])[{i, j, k}] - Fresh(i * hx, j * hy, k * hz)) > e)
					{
						f = 0;
					}
				}
			}
		}
		
		if (threadRank == 0)
		{
			iter++;
		}

		int tmpF;
		MPI_Allreduce(&f, &tmpF, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
		f = tmpF;
	} while (f == 0);
	

	double finish = MPI_Wtime();
	if (threadRank == 0)
	{
		std::cout << "iterations: " << iter << std::endl;
		printf("Time: %lf\n", finish - start);
	}
	

	double max = 0.0;
	for (int i = F[0]->localStartInDim(0) + 1; i < F[0]->localEndInDim(0) - 1; i++)
	{
		for (int j = 1; j < Jm; j++)
		{
			for (int k = 1; k < Km; k++)
			{
				double fff1 = fabs((*F[L1])[{i, j, k}] - Fresh(i * hx, j * hy, k * hz));
				if (fff1 > max)
				{
					max = fff1;
				}
			}
		}
	}
	double tmpMax = 0;
	MPI_Allreduce(&max, &tmpMax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	if (threadRank == 0)
	{
		max = tmpMax;
		printf("Max differ = %lf\n", max);
	}

	
	
	delete[] buffer[0];
	delete[] buffer[1];
	F[0]->deallocate();
	F[1]->deallocate();
	controller.terminate();	
	MPI_Finalize();
	return 0;
}
